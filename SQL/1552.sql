SELECT  biblio.title, items.barcode, items.holdingbranch, issues.date_due 
FROM items 
LEFT JOIN biblio ON (items.biblionumber=biblio.biblionumber) 
LEFT JOIN issues ON (items.itemnumber=issues.itemnumber) 
WHERE  items.homebranch LIKE <<Choose Library|SLIBS>>
   AND items.holdingbranch <> <<Choose Library|SLIBS>>
   AND issues.date_due != ''
   AND issues.date_due is not NULL 
   AND issues.date_due < DATE_SUB(curdate(), INTERVAL <<Enter days overdue (XX)>> DAY)  
ORDER BY  items.holdingbranch