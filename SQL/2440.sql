SELECT GROUP_CONCAT(DISTINCT r.reservedate) AS HoldDate,
       CONCAT('<a target="_blank" href="/cgi-bin/koha/catalogue/detail.pl?biblionumber=',b.biblionumber,'\">',b.title,'</a>') AS Title,
       GROUP_CONCAT(DISTINCT CONCAT('<a target="_blank" href=\"/cgi-bin/koha/circ/circulation.pl?borrowernumber=',p.borrowernumber,'\">',p.cardnumber,'</a>')) AS Cardnumber,
       GROUP_CONCAT(DISTINCT CONCAT(' ', p.firstname, ' ', p.surname)) AS Patron,
       COUNT(DISTINCT CASE
                          WHEN (i.withdrawn <> '0'
                                OR i.itemlost <> '0'
                                OR i.damaged<> '0'
                                OR i.notforloan > '0'
                                OR i.itype = 'DC') THEN i.itemnumber
                          ELSE NULL
                      END) AS StatusCount,
       COUNT(DISTINCT i.itemnumber) AS ItemCount,
       COUNT(DISTINCT r.reserve_id) AS HoldCount
FROM biblio b
LEFT JOIN items i ON (i.biblionumber = b.biblionumber)
LEFT JOIN reserves r ON (b.biblionumber = r.biblionumber)
LEFT JOIN borrowers p ON (r.borrowernumber=p.borrowernumber)
WHERE r.branchcode LIKE <<Choose Library|SLIBS>>
GROUP BY b.biblionumber
HAVING ((StatusCount >= ItemCount) OR (ItemCount = 0))
ORDER BY HoldDate
LIMIT 10000