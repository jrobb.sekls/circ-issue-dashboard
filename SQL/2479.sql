SELECT CONCAT('<a href=\"/cgi-bin/koha/catalogue/detail.pl?biblionumber=',i.biblionumber,'\" target="_blank">'"View Record"'</a>') AS "link",
       i.barcode,
       i.homebranch,
       i.holdingbranch,
       b.title,
       b.author,
       i.itemcallnumber,
       i.itemnotes,
       i.itemnotes_nonpublic,
       i.datelastseen
FROM items i
LEFT JOIN biblio b ON i.biblionumber=b.biblionumber
WHERE i.homebranch <> i.holdingbranch
  AND i.onloan IS NULL
  AND i.homebranch <> 'ROTATION'
  AND i.homebranch <> 'SEKLS'
  AND ((i.homebranch LIKE <<Choose Library|SLIBS>>)
       OR i.holdingbranch LIKE <<Choose Library|SLIBS>>)
  AND i.itemlost = '0'
  AND i.itemnumber NOT IN
    ( SELECT itemnumber
     FROM branchtransfers
     WHERE datearrived IS NULL)
  AND i.itemnumber NOT IN
    ( SELECT itemnumber
     FROM reserves
     WHERE waitingdate IS NOT NULL)
  AND i.itemnumber NOT IN
    ( SELECT itemnumber
     FROM reserves
     WHERE found = 'T')
ORDER BY i.itemcallnumber