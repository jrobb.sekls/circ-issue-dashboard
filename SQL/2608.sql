SELECT i.barcode, b.title, i.itemcallnumber, i.holdingbranch AS 'current location', h.branchcode AS 'pickup library', 
h.waitingdate AS 'waiting since',
IF(((TO_DAYS(curdate())-TO_DAYS(h.waitingdate))> 7),CONCAT('<span style="color:red; font-weight:bold">',(TO_DAYS(curdate())-TO_DAYS(h.waitingdate)),'</span>'), IF(((TO_DAYS(curdate())-TO_DAYS(h.waitingdate))<= 7),CONCAT('<span style="color:black">',(TO_DAYS(curdate())-TO_DAYS(h.waitingdate)),'</span>'),"")) AS 'days waiting'
FROM reserves h 
LEFT JOIN borrowers p USING (borrowernumber) 
LEFT JOIN items i USING (itemnumber) 
LEFT JOIN biblio b ON (i.biblionumber=b.biblionumber) 
WHERE h.waitingdate IS NOT NULL
     AND i.homebranch LIKE <<Choose Library|SLIBS>>
     AND i.homebranch <> h.branchcode
ORDER BY h.waitingdate
LIMIT 5000