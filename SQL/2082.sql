SELECT 
b.datesent,
       items.datelastseen,
       b.frombranch,
       b.tobranch,
       items.homebranch,
       items.barcode,
       biblio.title,
       biblio.author,
       items.itemcallnumber,
       DATEDIFF(b.datesent, (DATE_SUB(curdate(), INTERVAL 5 DAY))) * -1 AS dayslate
FROM branchtransfers AS b
LEFT JOIN items ON (b.itemnumber = items.itemnumber)
LEFT JOIN biblioitems ON (biblioitems.biblionumber = items.biblionumber)
LEFT JOIN biblio ON (biblio.biblionumber = biblioitems.biblionumber)
WHERE b.frombranch LIKE <<Choose Library|SLIBS>>
  AND b.datesent IS NOT NULL
  AND b.datearrived IS NULL
  AND b.datecancelled IS NULL
  AND items.homebranch <> 'ROTATION'
  AND DATEDIFF(b.datesent, (DATE_SUB(curdate(), INTERVAL 5 DAY))) * -1 > '0'
GROUP BY b.branchtransfer_id
ORDER BY dayslate DESC
