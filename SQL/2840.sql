SELECT borrowers.cardnumber,
       borrowers.firstname,
       borrowers.surname,
       borrowers.phone,
       borrowers.email,
       items.barcode,
       biblio.title,
       items.itemcallnumber,
       items.homebranch AS "owner",
       issues.date_due,
       items.holdingbranch AS "transactingLocation",
        DATEDIFF(issues.date_due, (DATE_SUB(curdate(), INTERVAL 0 DAY))) * -1 AS daysOverdue,
        items.replacementprice,
        a.lib AS 'Lost'
FROM items
LEFT JOIN biblio on
(items.biblionumber=biblio.biblionumber
)
LEFT JOIN issues on
(items.itemnumber=issues.itemnumber
)
LEFT JOIN borrowers on
(issues.borrowernumber=borrowers.borrowernumber AND borrowers.branchcode LIKE <<Choose Library|SLIBS>>
)
LEFT JOIN authorised_values a
ON (a.authorised_value=items.itemlost)
WHERE (items.homebranch LIKE <<Choose Library|SLIBS>> OR items.holdingbranch LIKE <<Choose Library|SLIBS>>)
AND a.category = 'LOST'
AND (issues.date_due <> '' AND issues.date_due IS NOT NULL AND issues.date_due < DATE_SUB(curdate(), INTERVAL '99' DAY) OR items.itemlost = '2') 
ORDER BY items.homebranch