SELECT CONCAT('<a href=\"/cgi-bin/koha/catalogue/moredetail.pl?itemnumber=',items.itemnumber,'&biblionumber=',biblio.biblionumber,'&bi=',biblio.biblionumber,'#item',items.itemnumber,'\" target="_blank">'"link"'</a>') AS "View",
       items.barcode,
       biblio.title,
       biblio.author,
       a.lib AS 'lostStatus',
       items.itemlost_on,
       items.itemcallnumber,
       a2.lib as 'location',
       items.datelastseen,
       items.datelastborrowed,
       MAX(old_issues.returndate) AS 'returnDate',
       items.price,
       items.homebranch AS 'Library',
       items.holdingbranch,
       items.issues AS 'Circs',
       items.onloan AS due
FROM items
LEFT JOIN biblio ON (items.biblionumber = biblio.biblionumber)
LEFT JOIN old_issues ON (items.itemnumber = old_issues.itemnumber)
LEFT JOIN authorised_values a ON (items.itemlost=a.authorised_value)
LEFT JOIN authorised_values a2 ON (items.location=a2.authorised_value)
WHERE items.itemlost = <<Lost Status|LOST>>
  AND a.category='LOST'
  AND a2.category='LOC'
  AND items.homebranch LIKE <<Choose Library|SLIBS>>
GROUP BY items.itemnumber
ORDER BY items.itemcallnumber