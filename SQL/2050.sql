SELECT items.itemnumber,
       biblio.title,
       biblio.author,
       items.itemcallnumber,
       items.barcode,
       authorised_values.lib,
       items.homebranch,
       items.holdingbranch
FROM items
LEFT JOIN biblio ON (items.biblionumber=biblio.biblionumber)
LEFT JOIN authorised_values ON (items.itemlost=authorised_values.authorised_value)
WHERE items.itemlost <> '0'
  AND items.itemlost <> '2'
  AND authorised_values.category='LOST'
  AND items.homebranch <> items.holdingbranch
  AND items.homebranch LIKE <<Choose Library|SLIBS>>
ORDER BY items.holdingbranch