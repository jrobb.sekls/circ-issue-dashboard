SELECT 
       CONCAT('<a href=\"/cgi-bin/koha/catalogue/detail.pl?biblionumber=',items.biblionumber,'\" target="_blank">'"View"'</a>') AS "view",
       CONCAT('<a href=\"/cgi-bin/koha/cataloguing/additem.pl?op=edititem&biblionumber=',biblio.biblionumber,'&itemnumber=',items.itemnumber,'\" target="_blank" >',"Edit",'</a>') AS "edit",
       items.barcode,
       items.homebranch,
       biblio.title,
       biblio.author,
       items.itemcallnumber,
       IF((items.dateaccessioned < NOW() - INTERVAL 3 WEEK),CONCAT('<span style="color:red">',items.dateaccessioned,'</span>'),IF((items.dateaccessioned >= NOW() - INTERVAL 3 WEEK),CONCAT('<span style="color:black">',items.dateaccessioned,'</span>'),"")) AS DateAdded,
       authorised_values.lib AS status,
       items.onloan AS DueDate,
       items.issues,
       IF((COUNT(reserves.biblionumber > 0)), CONCAT('<span style="color:red"><b>',COUNT(reserves.biblionumber),'</b></span>'),IF((COUNT(reserves.biblionumber <= 0)), CONCAT('<span style="color:black">','0','</span>'),"")) AS HoldCount
FROM items
LEFT JOIN biblio ON (items.biblionumber=biblio.biblionumber)
LEFT JOIN reserves ON (biblio.biblionumber=reserves.biblionumber)
LEFT JOIN authorised_values ON (items.notforloan=authorised_values.authorised_value)
WHERE authorised_values.category='NOT_LOAN'
  AND items.homebranch LIKE <<Choose Library|SLIBS>>
  AND items.notforloan = '-1'
GROUP BY items.itemnumber
ORDER BY items.dateaccessioned
LIMIT 2000