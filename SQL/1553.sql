SELECT  biblio.title,  items.barcode,  items.homebranch,  issues.date_due,  
    borrowers.cardnumber,  borrowers.firstname,  borrowers.surname,  borrowers.phone,  borrowers.email 
FROM items 
LEFT JOIN  biblio on(items.biblionumber=biblio.biblionumber) 
LEFT JOIN  issues on(items.itemnumber=issues.itemnumber) 
LEFT JOIN  borrowers on(issues.borrowernumber=borrowers.borrowernumber) 
WHERE  items.homebranch != <<Choose Library|SLIBS>>
   AND  items.holdingbranch = <<Choose Library|SLIBS>>
   AND  issues.date_due != '' 
   AND  issues.date_due is not NULL 
   AND  issues.date_due < DATE_SUB(curdate(), INTERVAL <<Enter days overdue (nn)>> DAY)  
ORDER BY  items.holdingbranch