SELECT 
     DATE(s.date_due) as 'Due date',
     br1.branchname AS 'Transacting (responsible) library',
     CONCAT_WS(' -- ',
	    CONCAT ('<a target="_blank" href=\"/cgi-bin/koha/circ/circulation.pl?borrowernumber=',p.borrowernumber,'\" target="_blank">', CONCAT_WS (', ', p.surname, p.firstname, p.cardnumber),'</a>'),
        IF(LENGTH(p.email), CONCAT('[', '<a href="mailto:',p.email,'?subject=Overdue:%20',b.title,'">','email','</a>', ']'),NULL),
		IF(LENGTH(p.phone), p.phone,NULL)) AS 'Patron',
     br3.branchname AS 'Patron home library',
     CONCAT_WS (' , by ',CONCAT ('<a target="_blank" href=\"/cgi-bin/koha/catalogue/detail.pl?biblionumber=',b.biblionumber,'\" target="_blank">',UPPER(b.title),'</a>'), b.author) AS Title,
     CONCAT ('<a target="_blank" href=\"/cgi-bin/koha/catalogue/moredetail.pl?biblionumber=',b.biblionumber,'&itemnumber=',i.itemnumber,'#item',i.itemnumber,'\" target="_blank">', i.barcode,'</a>') AS 'Barcode',
     br2.branchname AS 'Item owner',
     i.itemcallnumber AS 'Call#', i.replacementprice AS 'Price'
FROM issues s
 JOIN items i on (s.itemnumber=i.itemnumber)
 JOIN biblio b on (i.biblionumber=b.biblionumber)
 JOIN borrowers p on (s.borrowernumber=p.borrowernumber)
 JOIN branches br1 ON (s.branchcode=br1.branchcode)
 JOIN branches br2 ON (i.homebranch=br2.branchcode)
 JOIN branches br3 ON (p.branchcode=br3.branchcode)
WHERE	    (s.branchcode = @ODUELIB:= <<Choose Library|SLIBS>> COLLATE utf8mb4_unicode_ci)
     AND	   s.date_due <= (curdate())   
ORDER BY      p.surname,  p.firstname,  i.itemcallnumber
LIMIT 7000