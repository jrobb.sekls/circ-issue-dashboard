SELECT  (@MASTERLIB:= <<library>> COLLATE utf8mb4_unicode_ci) AS 'report'
       ,'total' 
UNION ALL
SELECT  'Overdues From Other Libraries 1553 oda'
       ,CASE WHEN COUNT(*) > 0 THEN CONCAT('<a class="priority1" target="_blank" href="/cgi-bin/koha/reports/guided_reports.pl?reports=1553&phase=Run+this+report&param_name=Choose+Library%7CSLIBS&sql_params=',@MASTERLIB ,'&param_name=Enter+days+overdue+%28nn%29&sql_params=00">',COUNT(*),'</a>')
        ELSE 0 END AS total
FROM items
LEFT JOIN biblio
ON (items.biblionumber=biblio.biblionumber)
LEFT JOIN issues
ON (items.itemnumber=issues.itemnumber)
WHERE items.homebranch <> @MASTERLIB
AND items.holdingbranch LIKE @MASTERLIB
AND issues.date_due != ''
AND issues.date_due is not NULL
AND issues.date_due < DATE_SUB(curdate(), INTERVAL '00' DAY) 
UNION ALL
SELECT  'Long Overdue Lost From Other Libraries 2369 lodla'
       ,CASE WHEN COUNT(*) > 0 THEN CONCAT('<a class="priority1" target="_blank" href="/cgi-bin/koha/reports/guided_reports.pl?reports=2369&phase=Run+this+report&param_name=Choose+Library%7CSLIBS&sql_params=',@MASTERLIB ,'">',COUNT(*),'</a>')
        ELSE 0 END AS total
FROM items
LEFT JOIN biblio on
(items.biblionumber=biblio.biblionumber
)
LEFT JOIN issues on
(items.itemnumber=issues.itemnumber
)
LEFT JOIN borrowers on
(issues.borrowernumber=borrowers.borrowernumber
)
LEFT JOIN authorised_values a
ON (a.authorised_value=items.itemlost)
WHERE items.homebranch <> @MASTERLIB
AND items.holdingbranch = @MASTERLIB
AND a.category = 'LOST'
AND items.homebranch <> 'ROTATION'
AND (issues.date_due <> '' AND issues.date_due IS NOT NULL AND issues.date_due < DATE_SUB(curdate(), INTERVAL '99' DAY) OR items.itemlost = '2') 
UNION ALL
SELECT  'Overdues At Other Libraries 1552 odb'
       ,CASE WHEN COUNT(*) > 0 THEN CONCAT('<a class="priority2" target="_blank" href="/cgi-bin/koha/reports/guided_reports.pl?reports=1552&phase=Run+this+report&param_name=Choose+Library%7CSLIBS&sql_params=',@MASTERLIB ,'&param_name=Enter+days+overdue+%28XX%29&sql_params=00">',COUNT(*),'</a>')
        ELSE 0 END AS total
FROM items
LEFT JOIN biblio
ON (items.biblionumber=biblio.biblionumber)
LEFT JOIN issues
ON (items.itemnumber=issues.itemnumber)
WHERE items.homebranch LIKE @MASTERLIB
AND items.holdingbranch <> @MASTERLIB
AND issues.date_due != ''
AND issues.date_due is not NULL
AND issues.date_due < DATE_SUB(curdate(), INTERVAL '00' DAY) 
UNION ALL
SELECT  'Long Overdue Lost At Other Lib 2370 lodlb'
       ,CASE WHEN COUNT(*) > 0 THEN CONCAT('<a class="priority2" target="_blank" href="/cgi-bin/koha/reports/guided_reports.pl?reports=2370&phase=Run+this+report&param_name=Choose+Library%7CSLIBS&sql_params=',@MASTERLIB ,'">',COUNT(*),'</a>')
        ELSE 0 END AS total
FROM items
LEFT JOIN biblio on
(items.biblionumber=biblio.biblionumber
)
LEFT JOIN issues on
(items.itemnumber=issues.itemnumber
)
LEFT JOIN borrowers on
(issues.borrowernumber=borrowers.borrowernumber
)
LEFT JOIN authorised_values a
ON (a.authorised_value=items.itemlost)
WHERE items.homebranch LIKE @MASTERLIB
AND items.holdingbranch <> @MASTERLIB
AND a.category = 'LOST'
AND (issues.date_due <> '' AND issues.date_due IS NOT NULL AND issues.date_due < DATE_SUB(curdate(), INTERVAL '99' DAY) OR items.itemlost = '2') 
UNION ALL
SELECT  'Overdue Totals 2421 odc'
       ,CASE WHEN COUNT(*) > 0 THEN CONCAT('<a class="priority3" target="_blank" href="/cgi-bin/koha/reports/guided_reports.pl?reports=2421&phase=Run+this+report&param_name=Choose+Library%7CSLIBS&sql_params=',@MASTERLIB ,'">',COUNT(*),'</a>')
        ELSE 0 END AS total
       FROM issues s
WHERE s.branchcode LIKE @MASTERLIB
AND s.date_due <= (curdate()) 
UNION ALL
SELECT  'Long Overdue Lost Totals 2840 lodlc'
       ,CASE WHEN COUNT(*) > 0 THEN CONCAT('<a class="priority3" target="_blank" href="/cgi-bin/koha/reports/guided_reports.pl?reports=2840&phase=Run+this+report&param_name=Choose+Library%7CSLIBS&sql_params=',@MASTERLIB ,'">',COUNT(*),'</a>')
        ELSE 0 END AS total
FROM items
LEFT JOIN biblio on
(items.biblionumber=biblio.biblionumber
)
LEFT JOIN issues on
(items.itemnumber=issues.itemnumber
)
LEFT JOIN borrowers on
(issues.borrowernumber=borrowers.borrowernumber
)
LEFT JOIN authorised_values a
ON (a.authorised_value=items.itemlost)
WHERE (items.homebranch LIKE @MASTERLIB OR items.holdingbranch LIKE @MASTERLIB)
AND a.category = 'LOST'
AND (issues.date_due <> '' AND issues.date_due IS NOT NULL AND issues.date_due < DATE_SUB(curdate(), INTERVAL '99' DAY) OR items.itemlost = '2') 
UNION ALL
SELECT  'Transfers To Your Library transferstoreceive transto'
       ,CASE WHEN COUNT(*) > 0 THEN CONCAT('<a class="priority1" target="_blank" href="/cgi-bin/koha/circ/transferstoreceive.pl">',COUNT(*),'</a>')
        ELSE 0 END AS total
FROM branchtransfers AS b
LEFT JOIN items
ON (b.itemnumber = items.itemnumber)
LEFT JOIN biblioitems
ON (biblioitems.biblioitemnumber = items.biblioitemnumber)
LEFT JOIN biblio
ON (biblio.biblionumber = biblioitems.biblionumber)
WHERE b.tobranch LIKE @MASTERLIB
AND b.datearrived IS NULL
AND b.datesent IS NOT NULL
AND b.datecancelled IS NULL
AND items.homebranch <> 'ROTATION'
AND DATEDIFF(b.datesent, (DATE_SUB(curdate(), INTERVAL 5 DAY))) * -1 > '0' 
UNION ALL
SELECT  'Transfers From Your Library 2082 transfrom'
       ,CASE WHEN COUNT(*) > 0 THEN CONCAT('<a class="priority1" target="_blank" href="/cgi-bin/koha/reports/guided_reports.pl?reports=2082&phase=Run+this+report&param_name=Choose+Library%7CSLIBS&sql_params=',@MASTERLIB ,'">',COUNT(*),'</a>')
        ELSE 0 END AS total
FROM branchtransfers AS b
LEFT JOIN items
ON (b.itemnumber = items.itemnumber)
LEFT JOIN biblioitems
ON (biblioitems.biblioitemnumber = items.biblioitemnumber)
LEFT JOIN biblio
ON (biblio.biblionumber = biblioitems.biblionumber)
WHERE b.frombranch LIKE @MASTERLIB
AND b.datearrived IS NULL
AND b.datesent IS NOT NULL
AND b.datecancelled IS NULL
AND items.homebranch <> 'ROTATION'
AND DATEDIFF(b.datesent, (DATE_SUB(curdate(), INTERVAL 5 DAY))) * -1 > '0' 
UNION ALL
SELECT  'Awaiting Pickup At Your Library watitingreserves holdwaita'
       ,CASE WHEN COUNT(*) > 0 THEN CONCAT('<a class="priority1" target="_blank" href="/cgi-bin/koha/circ/waitingreserves.pl">',COUNT(*),'</a>')
        ELSE 0 END AS total
FROM reserves h
LEFT JOIN borrowers p USING
(borrowernumber
)
LEFT JOIN items i USING
(itemnumber
)
LEFT JOIN biblio b
ON (i.biblionumber=b.biblionumber)
WHERE h.waitingdate IS NOT NULL
AND h.branchcode LIKE @MASTERLIB 
UNION ALL
SELECT  'Awaiting Pickup At Other Libraries 2608 holdwaitb'
       ,CASE WHEN COUNT(*) > 0 THEN CONCAT('<a class="priority2" target="_blank" href="/cgi-bin/koha/reports/guided_reports.pl?reports=2608&phase=Run+this+report&param_name=Choose+Library%7CSLIBS&sql_params=',@MASTERLIB ,'">',COUNT(*),'</a>')
        ELSE 0 END AS total
FROM reserves h
LEFT JOIN borrowers p USING
(borrowernumber
)
LEFT JOIN items i USING
(itemnumber
)
LEFT JOIN biblio b
ON (i.biblionumber=b.biblionumber)
WHERE h.waitingdate IS NOT NULL
AND i.homebranch LIKE @MASTERLIB
AND i.homebranch <> h.branchcode
UNION ALL
SELECT 'Orphaned Holds 2440 orphold'
       ,CASE WHEN COUNT(*) > 0 THEN CONCAT('<a class="priority1" target="_blank" href="/cgi-bin/koha/reports/guided_reports.pl?reports=2440&phase=Run+this+report&param_name=Choose+Library%7CSLIBS&sql_params=',@MASTERLIB ,'">',COUNT(*),'</a>')
        ELSE 0 END AS total
FROM (
    SELECT GROUP_CONCAT(DISTINCT r.reservedate) AS HoldDate,
       CONCAT('<a target="_blank" href="/cgi-bin/koha/catalogue/detail.pl?biblionumber=',b.biblionumber,'\">',b.title,'</a>') AS Title,
       GROUP_CONCAT(DISTINCT CONCAT('<a target="_blank" href=\"/cgi-bin/koha/circ/circulation.pl?borrowernumber=',p.borrowernumber,'\">',p.cardnumber,'</a>')) AS Cardnumber,
       GROUP_CONCAT(DISTINCT CONCAT(' ', p.firstname, ' ', p.surname)) AS Patron,
       COUNT(DISTINCT CASE
                          WHEN (i.withdrawn <> '0'
                                OR i.itemlost <> '0'
                                OR i.damaged<> '0'
                                OR i.notforloan > '0'
                                OR i.itype = 'DC') THEN i.itemnumber
                          ELSE NULL
                      END) AS StatusCount,
       COUNT(DISTINCT i.itemnumber) AS ItemCount,
       COUNT(DISTINCT r.reserve_id) AS HoldCount
FROM biblio b
LEFT JOIN items i ON (i.biblionumber = b.biblionumber)
LEFT JOIN reserves r ON (b.biblionumber = r.biblionumber)
LEFT JOIN borrowers p ON (r.borrowernumber=p.borrowernumber)
WHERE r.branchcode LIKE @MASTERLIB
GROUP BY b.biblionumber
HAVING ((StatusCount >= ItemCount) OR (ItemCount = 0))
) oholds
UNION ALL
SELECT  'In Processing 2362 nflneg1'
       ,CASE WHEN COUNT(*) > 0 THEN CONCAT('<a class="priority2" target="_blank" href="/cgi-bin/koha/reports/guided_reports.pl?reports=2362&phase=Run+this+report&param_name=Choose+Library%7CSLIBS&sql_params=',@MASTERLIB ,'">',COUNT(*),'</a>')
        ELSE 0 END AS total
FROM items i
LEFT JOIN authorised_values av ON (i.notforloan=av.authorised_value)
WHERE i.notforloan = '-1' AND av.category = 'NOT_LOAN'
AND i.homebranch LIKE @MASTERLIB 
UNION ALL
SELECT  'Withdrawn 1443 wd1'
       ,CASE WHEN COUNT(*) > 0 THEN CONCAT('<a class="priority3" target="_blank" href="/cgi-bin/koha/reports/guided_reports.pl?reports=1443&phase=Run+this+report&param_name=Choose+Library%7CSLIBS&sql_params=',@MASTERLIB ,'">',COUNT(*),'</a>')
        ELSE 0 END AS total
FROM items i
WHERE i.withdrawn = '1'
AND i.homebranch LIKE @MASTERLIB 
UNION ALL
SELECT  'Missing 1744 lost4'
       ,CASE WHEN COUNT(*) > 0 THEN CONCAT('<a class="priority3" target="_blank" href="/cgi-bin/koha/reports/guided_reports.pl?reports=1744&phase=Run+this+report&param_name=Lost+Status%7CLOST&sql_params=4&param_name=Choose+Library%7CSLIBS&sql_params=',@MASTERLIB ,'">',COUNT(*),'</a>')
        ELSE 0 END AS total
FROM items i
WHERE i.itemlost = '4'
AND i.homebranch LIKE @MASTERLIB 
UNION ALL
SELECT  'Lost 1744 lost1'
       ,CASE WHEN COUNT(*) > 0 THEN CONCAT('<a class="priority4" target="_blank" href="/cgi-bin/koha/reports/guided_reports.pl?reports=1744&phase=Run+this+report&param_name=Lost+Status%7CLOST&sql_params=1&param_name=Choose+Library%7CSLIBS&sql_params=',@MASTERLIB ,'">',COUNT(*),'</a>')
        ELSE 0 END AS total
FROM items i
WHERE i.itemlost = '1'
AND i.homebranch LIKE @MASTERLIB 
UNION ALL
SELECT  'Lost And Charged 1744 lost7'
       ,CASE WHEN COUNT(*) > 0 THEN CONCAT('<a class="priority3" target="_blank" href="/cgi-bin/koha/reports/guided_reports.pl?reports=1744&phase=Run+this+report&param_name=Lost+Status%7CLOST&sql_params=7&param_name=Choose+Library%7CSLIBS&sql_params=',@MASTERLIB ,'">',COUNT(*),'</a>')
        ELSE 0 END AS total
FROM items i
WHERE i.itemlost = '7'
AND i.homebranch LIKE @MASTERLIB 
UNION ALL
SELECT  'Damaged And Charged 1744 lost9'
       ,CASE WHEN COUNT(*) > 0 THEN CONCAT('<a class="priority3" target="_blank" href="/cgi-bin/koha/reports/guided_reports.pl?reports=1744&phase=Run+this+report&param_name=Lost+Status%7CLOST&sql_params=9&param_name=Choose+Library%7CSLIBS&sql_params=',@MASTERLIB ,'">',COUNT(*),'</a>')
        ELSE 0 END AS total
FROM items i
WHERE i.itemlost = '9'
AND i.homebranch LIKE @MASTERLIB 
UNION ALL
SELECT  'Lost And Paid For 1744 lost3'
       ,CASE WHEN COUNT(*) > 0 THEN CONCAT('<a class="priority4" target="_blank" href="/cgi-bin/koha/reports/guided_reports.pl?reports=1744&phase=Run+this+report&param_name=Lost+Status%7CLOST&sql_params=3&param_name=Choose+Library%7CSLIBS&sql_params=',@MASTERLIB ,'">',COUNT(*),'</a>')
        ELSE 0 END AS total
FROM items i
WHERE i.itemlost = '3'
AND i.homebranch LIKE @MASTERLIB 
UNION ALL
SELECT  'Lost At Other Libraries 2050 lostabroad'
       ,CASE WHEN COUNT(*) > 0 THEN CONCAT('<a class="priority2" target="_blank" href="/cgi-bin/koha/reports/guided_reports.pl?reports=2050&phase=Run+this+report&param_name=Choose+Library%7CSLIBS&sql_params=',@MASTERLIB ,'">',COUNT(*),'</a>')
        ELSE 0 END AS total
FROM items
LEFT JOIN biblio
ON (items.biblionumber=biblio.biblionumber)
LEFT JOIN authorised_values
ON (items.itemlost=authorised_values.authorised_value)
WHERE items.itemlost <> '0'
AND items.itemlost <> '2'
AND authorised_values.category='LOST'
AND items.homebranch <> items.holdingbranch
AND items.homebranch LIKE @MASTERLIB 
UNION ALL
SELECT  'Available At Other Libraries 2479 availabad'
       ,CASE WHEN COUNT(*) > 0 THEN CONCAT('<a class="priority2" target="_blank" href="/cgi-bin/koha/reports/guided_reports.pl?reports=2479&phase=Run+this+report&param_name=Choose+Library%7CSLIBS&sql_params=',@MASTERLIB ,'">',COUNT(*),'</a>')
        ELSE 0 END AS total
FROM items i
LEFT JOIN biblio b
ON i.biblionumber=b.biblionumber
WHERE i.homebranch <> i.holdingbranch
AND i.onloan IS NULL
AND i.homebranch <> 'ROTATION'
AND i.homebranch <> 'SEKLS'
AND ((i.homebranch LIKE @MASTERLIB) OR i.holdingbranch LIKE @MASTERLIB)
AND i.itemlost = '0'
AND i.itemnumber NOT IN ( SELECT itemnumber FROM branchtransfers WHERE datearrived IS NULL)
AND i.itemnumber NOT IN ( SELECT itemnumber FROM reserves WHERE waitingdate IS NOT NULL)
AND i.itemnumber NOT IN ( SELECT itemnumber FROM reserves WHERE found = 'T')
LIMIT 10000