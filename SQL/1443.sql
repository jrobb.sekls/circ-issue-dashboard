SELECT items.barcode, biblio.title, biblio.author, items.itemcallnumber, items.withdrawn, items.withdrawn_on
FROM items
LEFT JOIN biblio ON (items.biblionumber=biblio.biblionumber)
WHERE items.withdrawn != 0
     AND items.homebranch = <<Choose Library|SLIBS>>
ORDER BY items.itemcallnumber