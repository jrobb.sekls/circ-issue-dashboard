// Circ Dashboard - pull data from report 
$(document).ready(function() {
  
  $('#intranet-circulation-home-html').insertAfter('#offline-circulation');

  var libcode = $('#logged-in-info-full .logged-in-branch-code').text();      
  
  $.getJSON("/cgi-bin/koha/svc/report?id=2837&sql_params=" + libcode + "&annotated=1" + new Date().getTime(), function(result){
    var oda = '<span style="text-align: right">' + result[1].total + '</span>';
    var lodla = '<span style="text-align: right">' + result[2].total + '</span>';
    var odb = '<span style="text-align: right">' + result[3].total + '</span>';
    var lodlb = '<span style="text-align: right">' + result[4].total + '</span>';
    var odc = '<span style="text-align: right">' + result[5].total + '</span>';
    var lodlc = '<span style="text-align: right">' + result[6].total + '</span>';
    var transto = '<span style="text-align: right">' + result[7].total + '</span>';
    var transfrom = '<span style="text-align: right">' + result[8].total + '</span>';
    var holdwaita = '<span style="text-align: right">' + result[9].total + '</span>';    
    var holdwaitb = '<span style="text-align: right">' + result[10].total + '</span>';
    var orphold = '<span style="text-align: right">' + result[11].total + '</span>';
    var nflneg1 = '<span style="text-align: right">' + result[12].total + '</span>';
    var wd1 = '<span style="text-align: right">' + result[13].total + '</span>';
    var lost4 = '<span style="text-align: right">' + result[14].total + '</span>';
    var lost1 = '<span style="text-align: right">' + result[15].total + '</span>';
    var lost7 = '<span style="text-align: right">' + result[16].total + '</span>';
    var lost9 = '<span style="text-align: right">' + result[17].total + '</span>';
    var lost3 = '<span style="text-align: right">' + result[18].total + '</span>';
    var lostabroad = '<span style="text-align: right">' + result[19].total + '</span>';
    var availabad = '<span style="text-align: right">' + result[20].total + '</span>';
    
    $("#oda").replaceWith(oda);
    $("#odb").replaceWith(odb);
    $("#odc").replaceWith(odc);
    $("#lodla").replaceWith(lodla);
    $("#lodlb").replaceWith(lodlb);
    $("#lodlc").replaceWith(lodlc);
    $("#lostabroad").replaceWith(lostabroad);
    $("#transfrom").replaceWith(transfrom);
    $("#transto").replaceWith(transto);
    $("#availabad").replaceWith(availabad);
    $("#holdwaita").replaceWith(holdwaita);
    $("#holdwaitb").replaceWith(holdwaitb);
    $("#orphold").replaceWith(orphold);
    $("#nflneg1").replaceWith(nflneg1);
    $("#wd1").replaceWith(wd1);
    $("#lost4").replaceWith(lost4);
    $("#lost1").replaceWith(lost1);
    $("#lost7").replaceWith(lost7);
    $("#lost9").replaceWith(lost9);
    $("#lost3").replaceWith(lost3);
    }
  );
});
