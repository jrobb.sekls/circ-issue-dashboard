# Circ Issue Dashboard

## SQL
- 2837-MainDashboardReport SQL feeds numbers into the dashboard
- Each additional SQL file is referenced within the main report so clicking the result takes you to a list associated with the count
- Numbers will need adjust within the main report to correspond to the additional reports

## HTML
- Goes in IntranetCirculationHomeHTML -- JS is used to move it lower on the page

## CSS
- Goes in IntranetUserCSS

## JS
- Goes in IntranetUserJS
- Swap in report number for main MainDashboardReport report



